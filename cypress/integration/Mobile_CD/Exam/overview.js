import Step_1_PO from '../../../support/pageObjects/Lead_Form/Step_1_PO';
import Step2_Coaching_PO from '../../../support/pageObjects/Lead_Form/Step2_Coaching_PO';
import Step4_Coaching_PO from '../../../support/pageObjects/Lead_Form/Step4_Coaching_PO';
import OTP_PO from '../../../support/pageObjects/Lead_Form/OTP_PO';
import Thank_You_PO from '../../../support/pageObjects/Lead_Form/Thank_You_PO';
import Exam_PO from '../../../support/pageObjects/Exam/Overview_PO';
/// <reference types="Cypress" />

describe('College Info Page Test Suit', () => {
  const step_1_PO = new Step_1_PO();
  const step2_Coaching_PO = new Step2_Coaching_PO();
  const step4_Coaching_PO = new Step4_Coaching_PO();
  const otp_PO = new OTP_PO();
  const thank_You_PO = new Thank_You_PO();
  const exam_PO = new Exam_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    exam_PO.visitPage();
  });
  it('Fill lead through sidebar apply now ', () => {
    cy.scrollTo(0, 500);
    exam_PO.get_apply_now_button().first().click({ force: true });
    step_1_PO.firstStepSubmission(
      data.name,
      data.email,
      data.mobile,
      data.city
    );
    step2_Coaching_PO.second_step_submission_min_ip();
    step4_Coaching_PO.fourthStepSubmissionMinIp();
    // sA_Step_4_PO.fourthStepSubmissionMinIp();
    otp_PO.enterOTP();
    thank_You_PO.submitQue(data.que);
  });

  it('Verify SSR and client navigation', () => {
    exam_PO.verify_SSR_and_client_nav();
  });
});
