import Stream_List_PO from '../../../support/pageObjects/Exam/Stream_List_PO';
/// <reference types="Cypress" />

describe('India Universities Page Test Suit', () => {
  const stream_List_PO = new Stream_List_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    stream_List_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    stream_List_PO.verify_SSR_and_client_nav();
  });
});
