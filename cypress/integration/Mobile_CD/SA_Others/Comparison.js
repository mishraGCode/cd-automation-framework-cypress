import Comparison_PO from '../../../support/pageObjects/SA_Others/Comparison_PO';
/// <reference types="Cypress" />

describe('Abroad comparison Page Test Suit', () => {
  const comparison_PO = new Comparison_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    comparison_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    comparison_PO.verify_SSR_and_client_nav();
  });
});
