import Scholarship_List_PO from '../../../support/pageObjects/SA_Others/Scholarship_List_PO';
/// <reference types="Cypress" />

describe('Canada article Page Test Suit', () => {
  const scholarship_List_PO = new Scholarship_List_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    scholarship_List_PO.visitPage();
  });

  it('Verify SSR and client navigation', () => {
    scholarship_List_PO.verify_SSR_and_client_nav();
  });
});
