import Article_Canada_PO from '../../../support/pageObjects/SA_Others/Article_Canada_PO';
/// <reference types="Cypress" />

describe('Canada article Page Test Suit', () => {
  const article_Canada_PO = new Article_Canada_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    article_Canada_PO.visitPage();
  });

  it('Verify SSR and client navigation', () => {
    article_Canada_PO.verify_SSR_and_client_nav();
  });
});
