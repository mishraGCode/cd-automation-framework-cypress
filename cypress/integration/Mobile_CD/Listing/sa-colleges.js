import SA_Colleges_PO from '../../../support/pageObjects/Listing/SA_Colleges_PO';
/// <reference types="Cypress" />

describe('Study Abroad Colleges Page Test Suit', () => {
  const sA_Colleges_PO = new SA_Colleges_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    sA_Colleges_PO.visitPage();
  });
  it('Test SSR and client navigation', () => {
    sA_Colleges_PO.verify_SSR_and_client_nav();
  });
});
