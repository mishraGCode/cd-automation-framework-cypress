import India_Colleges_PO from '../../../support/pageObjects/Listing/India_Colleges_PO';
/// <reference types="Cypress" />

describe('India Universities Page Test Suit', () => {
  const india_Colleges_PO = new India_Colleges_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    india_Colleges_PO.visitPage();
  });
  it('Test SSR and client navigation', () => {
    india_Colleges_PO.verify_SSR_and_client_nav();
  });
});
