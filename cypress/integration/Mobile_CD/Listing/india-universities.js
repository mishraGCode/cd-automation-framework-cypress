import India_Universities_PO from '../../../support/pageObjects/Listing/india-universities_PO';
/// <reference types="Cypress" />

describe('India Universities Page Test Suit', () => {
  const india_Universities_PO = new India_Universities_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    india_Universities_PO.visitPage();
  });
  it('Test SSR and client navigation', () => {
    india_Universities_PO.verify_SSR_and_client_nav();
  });
});
