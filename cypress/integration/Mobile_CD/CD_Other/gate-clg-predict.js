import Gate_Clg_predict_PO from '../../../support/pageObjects/CD_Other/Gate_Gate_Clg_predict_PO';
/// <reference types="Cypress" />

describe('QNA Home Page Test Suit', () => {
  const gate_Clg_predict_PO = new Gate_Clg_predict_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    gate_Clg_predict_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    gate_Clg_predict_PO.verify_SSR_and_client_nav();
  });
});
