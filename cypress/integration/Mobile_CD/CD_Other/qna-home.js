import QNA_HOME_PO from '../../../support/pageObjects/CD_Other/QNA_HOME_PO';
/// <reference types="Cypress" />

describe('QNA Home Page Test Suit', () => {
  const qNA_HOME_PO = new QNA_HOME_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    qNA_HOME_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    qNA_HOME_PO.verify_SSR_and_client_nav();
  });
});
