import Clg_Predict_PO from '../../../support/pageObjects/CD_Other/Clg_Predict_PO';
/// <reference types="Cypress" />

describe('QNA Home Page Test Suit', () => {
  const clg_Predict_PO = new Clg_Predict_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    clg_Predict_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    clg_Predict_PO.verify_SSR_and_client_nav();
  });
});
