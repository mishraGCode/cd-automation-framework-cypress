import The_Print_PO from '../../../support/pageObjects/CD_Other/The_Print_PO';
/// <reference types="Cypress" />

describe('The Print Page Test Suit', () => {
  const the_Print_PO = new The_Print_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    the_Print_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    the_Print_PO.verify_SSR_and_client_nav();
  });
});
