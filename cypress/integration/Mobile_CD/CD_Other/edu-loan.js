import Edu_Loan_PO from '../../../support/pageObjects/CD_Other/Edu_Loan_PO';
/// <reference types="Cypress" />

describe('Education Loan Page Test Suit', () => {
  const edu_Loan_PO = new Edu_Loan_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    edu_Loan_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    edu_Loan_PO.verify_SSR_and_client_nav();
  });
});
