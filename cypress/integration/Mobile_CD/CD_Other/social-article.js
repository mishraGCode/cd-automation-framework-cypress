import Social_Article_PO from '../../../support/pageObjects/CD_Other/Social_Article_PO';
/// <reference types="Cypress" />

describe('Social Article Page Test Suit', () => {
  const social_Article_PO = new Social_Article_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    social_Article_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    social_Article_PO.verify_SSR_and_client_nav();
  });
});
