import Social_Home_PO from '../../../support/pageObjects/CD_Other/Social_Home_PO';
/// <reference types="Cypress" />

describe('Social Home Page Test Suit', () => {
  const social_Home_PO = new Social_Home_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    social_Home_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    social_Home_PO.verify_SSR_and_client_nav();
  });
});
