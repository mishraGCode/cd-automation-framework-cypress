import Step_1_PO from '../../../support/pageObjects/Lead_Form/Step_1_PO';
import SA_Step_2_PO from '../../../support/pageObjects/Lead_Form/SA_Step_2_PO';
import SA_Step_4_PO from '../../../support/pageObjects/Lead_Form/SA_Step_4_PO';
import OTP_PO from '../../../support/pageObjects/Lead_Form/OTP_PO';
import Thank_You_PO from '../../../support/pageObjects/Lead_Form/Thank_You_PO';
import Info_PO from '../../../support/pageObjects/Institutes/Info_PO';
/// <reference types="Cypress" />

describe('College Info Page Test Suit', () => {
  const step_1_PO = new Step_1_PO();
  const sA_Step_2_PO = new SA_Step_2_PO();
  const sA_Step_4_PO = new SA_Step_4_PO();
  const otp_PO = new OTP_PO();
  const thank_You_PO = new Thank_You_PO();
  const info_PO = new Info_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    info_PO.visitPage();
  });
  it('Fill lead through sidebar apply now ', () => {
    info_PO.getSidebarAplyNwBtn().first().click({ force: true });
    step_1_PO.firstStepSubmission(
      data.name,
      data.email,
      data.mobile,
      data.city
    );
    // sA_Step_2_PO.secondStepSubmissionMinIp();
    // sA_Step_3_PO.thirdStepSubmission(
    //   data.tenthBoard,
    //   data.tenthPer,
    //   data.tenthSchoolName,
    //   data.twelthBoard,
    //   data.twelthPer,
    //   data.twelthSchoolName,
    //   data.twelthSpecialization
    // );
    sA_Step_4_PO.fourthStepSubmissionMinIp();
    otp_PO.enterOTP();
    thank_You_PO.submitQue(data.que);
  });

  it.only('Test SSR and client navigation', () => {
    info_PO.verify_SSR_and_client_nav();
  });
});
