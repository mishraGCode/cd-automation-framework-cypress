import Home_PO from '../../../support/pageObjects/Courses/Home_PO';
/// <reference types="Cypress" />

describe('Course Home Page Test Suit', () => {
  const home_PO = new Home_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    home_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    home_PO.verify_SSR_and_client_nav();
  });
});
