import Level_PO from '../../../support/pageObjects/Courses/Level_PO';
/// <reference types="Cypress" />

describe('Course Slug Listing Page Test Suit', () => {
  const level_PO = new Level_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    level_PO.visitPage();
  });

  it('Verify SSR and client navigation', () => {
    level_PO.verify_SSR_and_client_nav();
  });
});
