import Stream_Level_PO from '../../../support/pageObjects/Courses/Stream_Level_PO';
/// <reference types="Cypress" />

describe('Course Slug Listing Page Test Suit', () => {
  const stream_Level_PO = new Stream_Level_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    stream_Level_PO.visitPage();
  });

  it('Verify SSR and client navigation', () => {
    stream_Level_PO.verify_SSR_and_client_nav();
  });
});
