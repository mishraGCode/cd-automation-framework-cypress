import Slug_Listing_PO from '../../../support/pageObjects/Courses/Slug_Listing_PO';
/// <reference types="Cypress" />

describe('Course Slug Listing Page Test Suit', () => {
  const slug_Listing_PO = new Slug_Listing_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    slug_Listing_PO.visitPage();
  });

  it('Verify SSR and client navigation', () => {
    slug_Listing_PO.verify_SSR_and_client_nav();
  });
});
