import Overview_PO from '../../../support/pageObjects/Courses/Overview_PO';
/// <reference types="Cypress" />

describe('College News Page Test Suit', () => {
  const overview_PO = new Overview_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    overview_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    overview_PO.verify_SSR_and_client_nav();
  });
});
