import Sub_Article_PO from '../../../support/pageObjects/Admission/Sub_Article_PO';
/// <reference types="Cypress" />

describe('Admission Sub Article Page Test Suit', () => {
  const sub_Article_PO = new Sub_Article_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    sub_Article_PO.visitPage();
  });

  it('Verify SSR and client navigation', () => {
    sub_Article_PO.verify_SSR_and_client_nav();
  });
});
