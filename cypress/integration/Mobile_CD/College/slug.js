import Slug_PO from '../../../support/pageObjects/College/Slug_PO';
/// <reference types="Cypress" />

describe('College Slug Page Test Suit', () => {
  const slug_PO = new Slug_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    slug_PO.visitPage();
  });

  it('Verify SSR and client navigation', () => {
    slug_PO.verify_SSR_and_client_nav();
  });
});
