import Course_PO from '../../../support/pageObjects/College/Course_PO';
/// <reference types="Cypress" />

describe('College Course Page Test Suit', () => {
  const course_PO = new Course_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    course_PO.visitPage();
  });

  it('Verify SSR and client navigation', () => {
    course_PO.verify_SSR_and_client_nav();
  });
});
