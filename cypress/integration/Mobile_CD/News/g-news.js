import G_News_PO from '../../../support/pageObjects/News/G_News_PO';
/// <reference types="Cypress" />

describe('Global News Page Test Suit', () => {
  const g_News_PO = new G_News_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    g_News_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    g_News_PO.verify_SSR_and_client_nav();
  });
});
