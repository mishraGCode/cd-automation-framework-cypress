import E_News_PO from '../../../support/pageObjects/News/E_News_PO';
/// <reference types="Cypress" />

describe('Exam News Page Test Suit', () => {
  const e_News_PO = new E_News_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    e_News_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    e_News_PO.verify_SSR_and_client_nav();
  });
});
