import Alert_PO from '../../../support/pageObjects/News/Alert_PO';
/// <reference types="Cypress" />

describe('Alert News Page Test Suit', () => {
  const alert_PO = new Alert_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    alert_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    alert_PO.verify_SSR_and_client_nav();
  });
});
