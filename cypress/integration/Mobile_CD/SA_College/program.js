import Program_PO from '../../../support/pageObjects/SA_College/Program_PO';
/// <reference types="Cypress" />

describe('SA program listing Page Test Suit', () => {
  const program_PO = new Program_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    program_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    program_PO.verify_SSR_and_client_nav();
  });
});
