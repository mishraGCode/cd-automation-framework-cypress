import Course_And_fees_PO from '../../../support/pageObjects/SA_College/Course_&_Fees_PO';
/// <reference types="Cypress" />

describe('SA program listing Page Test Suit', () => {
  const course_And_fees_PO = new Course_And_fees_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    course_And_fees_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    course_And_fees_PO.verify_SSR_and_client_nav();
  });
});
