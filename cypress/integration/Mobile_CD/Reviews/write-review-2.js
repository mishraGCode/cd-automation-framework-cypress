import Write_Review_2_PO from '../../../support/pageObjects/Reviews/Write_Review_2_PO';
/// <reference types="Cypress" />

describe('Write Review Two Page Test Suit', () => {
  const write_Review_2_PO = new Write_Review_2_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    write_Review_2_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    write_Review_2_PO.verify_SSR_and_client_nav();
  });
});
