import CD_Review_PO from '../../../support/pageObjects/Reviews/CD_Review_PO';
/// <reference types="Cypress" />

describe('Write Review Two Page Test Suit', () => {
  const cD_Review_PO = new CD_Review_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    cD_Review_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    cD_Review_PO.verify_SSR_and_client_nav();
  });
});
