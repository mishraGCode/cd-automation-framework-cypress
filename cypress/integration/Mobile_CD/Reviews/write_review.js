import Write_Review_PO from '../../../support/pageObjects/Reviews/Write_Review_PO';
/// <reference types="Cypress" />

describe('Write Review Page Test Suit', () => {
  const write_Review_PO = new Write_Review_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    write_Review_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    write_Review_PO.verify_SSR_and_client_nav();
  });
});
