import Btech_Delhi_PO from '../../../support/pageObjects/Home/Btech_Delhi_PO';
/// <reference types="Cypress" />

describe('B.Tech Delhi home page Test Suit', () => {
  const btech_Delhi_PO = new Btech_Delhi_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    btech_Delhi_PO.visitPage();
  });
  it('Test SSR and client navigation', () => {
    btech_Delhi_PO.verifySsrClientNav();
  });
});
