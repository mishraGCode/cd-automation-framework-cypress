import SA_PO from '../../../support/pageObjects/Home/SA_PO';
/// <reference types="Cypress" />

describe('Study Abroad Home Page Test Suit', () => {
  const sA_PO = new SA_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    sA_PO.visitPage();
  });
  it('Verify SSR and client navigation', () => {
    sA_PO.verify_SSR_and_client_nav();
  });
});
