import Default_PO from '../../../support/pageObjects/Home/default_PO';
/// <reference types="Cypress" />

describe('CD default home page Test Suit', () => {
  const default_PO = new Default_PO();

  before(function () {
    cy.fixture('SA_User').then(function (data) {
      //this.data = data;
      globalThis.data = data;
    });
  });

  beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    default_PO.visitPage();
  });
  it('Test SSR and client navigation', () => {
    default_PO.verifySsrClientNav();
  });
});
