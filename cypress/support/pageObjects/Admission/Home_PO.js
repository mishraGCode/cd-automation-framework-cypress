class Home_PO {
  get_next_page_button() {
    return cy.xpath("//a[contains(@class,'next')]");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/admission');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_next_page_button(), null);
    this.get_next_page_button().click({ force: true });
    cy.url().should('include', '2');
    cy.go('back');
    cy.ValidateElement(this.get_next_page_button(), null);
    cy.reload();
    cy.ValidateElement(this.get_next_page_button(), null);
  }
}
export default Home_PO;
