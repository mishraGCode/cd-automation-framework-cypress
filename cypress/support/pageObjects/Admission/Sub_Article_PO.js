class Sub_Article_PO {
  getSidebarAplyNwBtn() {
    return cy.xpath("//span[normalize-space()='Apply Now']");
  }

  get_reviews_silo() {
    return cy.xpath("//*[normalize-space()='Reviews']");
  }

  visitPage() {
    if (
      Cypress.env('stage') === 'https://collegedunia.com' ||
      Cypress.env('stage') === 'https://beta2.collegedunia.com'
    ) {
      cy.visit(
        Cypress.env('stage') +
          '/admission/107-du-ug-admission-2022-registration-extended-till-may-31-courses-entrance-exam-eligibility-application-process'
      );
    } else {
      cy.visit(
        Cypress.env('stage') + '/admission/6002-iit-bombay-admission-details'
      );
    }
  }

  verify_SSR_and_client_nav() {
    this.get_reviews_silo().should('contain', 'Reviews');
    this.get_reviews_silo().last().click({ force: true });
    cy.url().should('include', 'reviews');
    cy.go('back');
    this.get_reviews_silo().should('contain', 'Reviews');
    cy.reload();
    this.get_reviews_silo().should('contain', 'Reviews');
  }
}
export default Sub_Article_PO;
