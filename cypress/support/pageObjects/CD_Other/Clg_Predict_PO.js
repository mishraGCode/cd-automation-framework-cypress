class Clg_Predict_PO {
  get_Neet_Link() {
    return cy.xpath("//a[normalize-space()='NEET 2022 College Predictor']");
  }
  visitPage() {
    cy.visit(Cypress.env('stage') + '/college-predictor');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_Neet_Link(), null);
    this.get_Neet_Link().click({ force: true });
    cy.url().should('include', 'career');
    cy.go('back');
    cy.ValidateElement(this.get_Neet_Link(), null);
    cy.reload();
    cy.ValidateElement(this.get_Neet_Link(), null);
  }
}
export default Clg_Predict_PO;
