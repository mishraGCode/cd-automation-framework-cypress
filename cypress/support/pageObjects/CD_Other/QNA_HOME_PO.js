class QNA_Home_PO {
  get_heading() {
    return cy.xpath(
      "//*[contains(text(),'Get Answers on Test Preparation, Admissions & Camp')]"
    );
  }
  visitPage() {
    cy.visit(Cypress.env('stage') + '/qna');
  }

  getCareerLink() {
    if (Cypress.config('configFile') === 'cypress_mob.json') {
      return cy.xpath("//*[normalize-space()='Careers']");
    } else {
      return cy.xpath("//*[normalize-space()='career']");
    }
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_heading(), null);
    this.getCareerLink().last().click({ force: true });
    cy.url().should('include', 'career');
    cy.go('back');
    cy.ValidateElement(this.get_heading(), null);
    cy.reload();
    cy.ValidateElement(this.get_heading(), null);
  }
}
export default QNA_Home_PO;
