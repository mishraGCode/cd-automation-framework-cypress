class Edu_Loan_PO {
  get_compare_loan_link() {
    return cy.xpath("//a[normalize-space()='Compare Loans']");
  }
  visitPage() {
    cy.visit(Cypress.env('stage') + '/education-loan');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_compare_loan_link(), null);
    this.get_compare_loan_link().click({ force: true });
    cy.url().should('include', 'compare-loan');
    cy.go('back');
    cy.ValidateElement(this.get_compare_loan_link(), null);
    cy.reload();
    cy.ValidateElement(this.get_compare_loan_link(), null);
  }
}
export default Edu_Loan_PO;
