class Gate_Clg_Predict_PO {
  get_gate_breadcrumb() {
    return cy.xpath("//span[normalize-space()='GATE']");
  }
  visitPage() {
    cy.visit(Cypress.env('stage') + '/gate-college-predictor');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_gate_breadcrumb(), null);
    this.get_gate_breadcrumb().click({ force: true });
    cy.url().should('include', 'career');
    cy.go('back');
    cy.ValidateElement(this.get_gate_breadcrumb(), null);
    cy.reload();
    cy.ValidateElement(this.get_gate_breadcrumb(), null);
  }
}
export default Gate_Clg_Predict_PO;
