class Social_Article_PO {
  get_heading() {
    return cy.xpath(
      "//h1[contains(text(),'How To Get Thor’s Rugged Bearded Look From Infinit')]"
    );
  }
  visitPage() {
    cy.visit(
      Cypress.env('stage') +
        '/social/8-how-to-get-thors-rugged-bearded-look-from-infinity-wars'
    );
  }

  get_social_breadcrumb() {
    return cy.xpath("//span[normalize-space()='Social']");
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_heading(), null);
    this.get_social_breadcrumb().click({ force: true });
    cy.url().should('include', 'social');
    cy.go('back');
    cy.ValidateElement(this.get_heading(), null);
    cy.reload();
    cy.ValidateElement(this.get_heading(), null);
  }
}
export default Social_Article_PO;
