class Social_Home_PO {
  get_lifestyle_link() {
    return cy.xpath("//div[normalize-space()='Lifestyle']");
  }
  visitPage() {
    cy.visit(Cypress.env('stage') + '/social');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_lifestyle_link(), null);
    this.get_lifestyle_link().last().click({ force: true });
    cy.url().should('include', 'lifestyle');
    cy.go('back');
    cy.ValidateElement(this.get_lifestyle_link(), null);
    cy.reload();
    cy.ValidateElement(this.get_lifestyle_link(), null);
  }
}
export default Social_Home_PO;
