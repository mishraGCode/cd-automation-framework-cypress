class The_Print_PO {
  get_mba_link() {
    return cy.xpath(
      "//a[@href='/courses/master-of-business-administration-mba']"
    );
  }
  visitPage() {
    cy.visit(Cypress.env('stage') + '/theprint');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_mba_link(), null);
    this.get_mba_link().click({ force: true });
    cy.url().should(
      'include',
      '/courses/master-of-business-administration-mba'
    );
    cy.go('back');
    cy.ValidateElement(this.get_mba_link(), null);
    cy.reload();
    cy.ValidateElement(this.get_mba_link(), null);
  }
}
export default The_Print_PO;
