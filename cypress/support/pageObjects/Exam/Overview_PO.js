class Info_PO {
  get_apply_now_button() {
    return cy.xpath("//button[normalize-space()='APPLY NOW']");
  }

  get_results_silo() {
    return cy.get("a[href='/exams/jee-main/results']");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/exams/jee-main');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_results_silo(), null);
    //this.get_results_silo().should('contain', 'results');
    this.get_results_silo().first().click({ force: true });
    cy.url().should('include', 'results');
    cy.go('back');
    cy.ValidateElement(this.get_results_silo(), null);
    cy.reload();
    cy.ValidateElement(this.get_results_silo(), null);
  }
}
export default Info_PO;
