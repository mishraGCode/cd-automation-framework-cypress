class Stream_List_PO {
  get_exams_breadcrumb() {
    return cy.xpath("//span[normalize-space()='Exams']");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/exams/engineering');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_exams_breadcrumb(), null);
    this.get_exams_breadcrumb().last().click({ force: true });
    cy.url().should('include', 'exams');
    cy.go('back');
    cy.ValidateElement(this.get_exams_breadcrumb(), null);
    cy.reload();
    cy.ValidateElement(this.get_exams_breadcrumb(), null);
  }
}
export default Stream_List_PO;
