class Home_PO {
  get_engineering() {
    return cy.xpath("//div[contains(text(),'engineering')]");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/exams');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_engineering(), null);
    this.get_engineering().click({ force: true });
    cy.url().should('include', 'engineering');
    cy.go('back');
    cy.ValidateElement(this.get_engineering(), null);
    cy.reload();
    cy.ValidateElement(this.get_engineering(), null);
  }
}
export default Home_PO;
