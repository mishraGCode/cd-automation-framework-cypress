class Slug_Listing_PO {
  get_mech_engg_link() {
    return cy.xpath(
      "//a[contains(text(),'Bachelor of Technology [B.Tech] (Mechanical Engineering)')]"
    );
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/courses/btech');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_mech_engg_link(), null);
    this.get_mech_engg_link().first().click({ force: true });
    cy.url().should('include', 'mechanical-engineering');
    cy.go('back');
    cy.ValidateElement(this.get_mech_engg_link(), null);
    cy.reload();
    cy.ValidateElement(this.get_mech_engg_link(), null);
  }
}
export default Slug_Listing_PO;
