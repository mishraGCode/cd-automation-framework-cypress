class Overview_PO {
  get_abroad_silo() {
    return cy.xpath("//a[normalize-space()='abroad']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') + '/courses/master-of-business-administration-mba'
    );
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_abroad_silo(), null);
    this.get_abroad_silo().click({ force: true });
    cy.url().should('include', 'abroad-studies');
    cy.go('back');
    cy.ValidateElement(this.get_abroad_silo(), null);
    cy.reload();
    cy.ValidateElement(this.get_abroad_silo(), null);
  }
}
export default Overview_PO;
