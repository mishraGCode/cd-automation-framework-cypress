class Level_PO {
  get_engineering() {
    return cy.xpath("//span[normalize-space()='Engineering']");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/courses/courses-after-12th');
  }

  verify_SSR_and_client_nav() {
    this.get_engineering().should('contain', 'Engineering');
    this.get_engineering().click({ force: true });
    cy.url().should('include', 'courses/engineering-courses-after-12th');
    cy.go('back');
    this.get_engineering().should('contain', 'Engineering');
    cy.reload();
    this.get_engineering().should('contain', 'Engineering');
  }
}
export default Level_PO;
