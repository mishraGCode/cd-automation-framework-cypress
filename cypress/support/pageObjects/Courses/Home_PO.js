class Home_PO {
  get_after_twelve_courses_link() {
    return cy.xpath("//h2[normalize-space()='After 10+2 Courses']");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/courses');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_after_twelve_courses_link(), null);
    this.get_after_twelve_courses_link().click({ force: true });
    cy.url().should('include', 'after');
    cy.go('back');
    cy.ValidateElement(this.get_after_twelve_courses_link(), null);
    cy.reload();
    cy.ValidateElement(this.get_after_twelve_courses_link(), null);
  }
}
export default Home_PO;
