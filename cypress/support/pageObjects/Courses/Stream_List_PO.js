class Stream_Listing_PO {
  get_heading() {
    return cy.xpath(
      "//h1[contains(text(),'Engineering Course: List, Without Math, After 12th')]"
    );
  }

  get_all_courses_silo() {
    if (Cypress.config('configFile') === 'cypress_mob.json') {
      return cy.xpath("//a[normalize-space()='all courses']");
    } else {
      return cy.xpath("//a[normalize-space()='All Courses']");
    }
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/courses/engineering');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_heading(), null);
    this.get_all_courses_silo().last().click({ force: true });
    cy.url().should('include', 'courses/engineering/all-courses');
    cy.go('back');
    cy.ValidateElement(this.get_heading(), null);
    cy.reload();
    cy.ValidateElement(this.get_heading(), null);
  }
}
export default Stream_Listing_PO;
