class Info_PO {
  getSidebarAplyNwBtn() {
    return cy.xpath("//span[normalize-space()='Apply Now']");
  }

  get_courseAndFess_silo() {
    return cy.xpath("//a[normalize-space()='Courses & Fees']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') +
        '/university/25787-lovely-professional-university-lpu-jalandhar'
    );
  }

  verify_SSR_and_client_nav() {
    this.get_courseAndFess_silo().should('contain', 'Courses');
    this.get_courseAndFess_silo().last().click({ force: true });
    cy.url().should('include', 'courses-fees');
    cy.go('back');
    this.get_courseAndFess_silo().should('contain', 'Courses');
    cy.reload();
    this.get_courseAndFess_silo().should('contain', 'Courses');
  }
}
export default Info_PO;
