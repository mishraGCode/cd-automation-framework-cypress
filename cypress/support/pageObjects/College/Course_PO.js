class Course_PO {
  getSidebarAplyNwBtn() {
    return cy.xpath("//span[normalize-space()='Apply Now']");
  }

  get_dist_edu_silo() {
    return cy.xpath("//a[normalize-space()='Distance Education']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') +
        '/university/25787-lovely-professional-university-lpu-jalandhar/courses-fees?course_id=2049'
    );
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_dist_edu_silo(), null);
    this.get_dist_edu_silo().last().click({ force: true });
    cy.url().should('include', 'distance-education');
    cy.go('back');
    cy.ValidateElement(this.get_dist_edu_silo(), null);
    cy.reload();
    cy.ValidateElement(this.get_dist_edu_silo(), null);
  }
}
export default Course_PO;
