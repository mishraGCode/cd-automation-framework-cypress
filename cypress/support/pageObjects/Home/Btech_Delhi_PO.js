class Btech_Delhi_PO {
  getTopClgHeading() {
    return cy.xpath(
      "//h2[normalize-space()='Top Universities/Colleges for BE/B.Tech']"
    );
  }

  getCareerLink() {
    return cy.xpath("//*[normalize-space()='Careers']");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/btech-courses?city_id=125');
  }

  verifySsrClientNav() {
    this.getTopClgHeading().should('contain', 'Top');
    this.getCareerLink().last().click({ force: true });
    cy.url().should('include', 'career');
    cy.go('back');
    this.getTopClgHeading().should('contain', 'Top');
    cy.reload();
    this.getTopClgHeading().should('contain', 'Top');
  }
}
export default Btech_Delhi_PO;
