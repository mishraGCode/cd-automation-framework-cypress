class SA_PO {
  get_heading_text() {
    return cy.xpath(
      "//*[normalize-space()='Find Study Abroad Universities and Programs']"
    );
  }

  get_science_stream_link() {
    return cy.xpath("//*[normalize-space()='Sciences']");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/study-abroad');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_heading_text(), null);
    this.get_science_stream_link().click({ force: true });
    cy.url().should('include', 'sciences-colleges');
    cy.go('back');
    cy.ValidateElement(this.get_heading_text(), null);
    cy.reload();
    cy.ValidateElement(this.get_heading_text(), null);
  }
}
export default SA_PO;
