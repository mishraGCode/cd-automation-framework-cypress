class Course_Fee_PO {
  get_info_silo() {
    return cy.xpath("//a[normalize-space()='Info']");
  }

  get_breadcrumb_institute() {
    return cy.xpath("//a[normalize-space()='Institutes']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') +
        '/institute/76-unique-ias-academy-new-delhi/courses-fees'
    );
  }

  verify_SSR_and_client_nav() {
    this.get_info_silo().should('contain', 'Info');
    this.get_breadcrumb_institute().click({ force: true });
    cy.url().should('include', 'institutes');
    cy.go('back');
    this.get_info_silo().should('contain', 'Info');
    cy.reload();
    this.get_info_silo().should('contain', 'Info');
  }
}
export default Course_Fee_PO;
