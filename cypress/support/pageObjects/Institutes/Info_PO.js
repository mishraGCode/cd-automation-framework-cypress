class Info_PO {
  get_courseAndFess_silo() {
    return cy.xpath("//a[normalize-space()='Courses & Fees']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') + '/institute/76-unique-ias-academy-new-delhi'
    );
  }

  verify_SSR_and_client_nav() {
    this.get_courseAndFess_silo().should('contain', 'Courses');
    this.get_courseAndFess_silo().last().click({ force: true });
    cy.url().should('include', 'courses-fees');
    cy.go('back');
    this.get_courseAndFess_silo().should('contain', 'Courses');
    cy.reload();
    this.get_courseAndFess_silo().should('contain', 'Courses');
  }
}
export default Info_PO;
