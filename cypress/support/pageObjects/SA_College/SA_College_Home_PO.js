class SA_College_Home_PO {
  getSidebarAplyNwBtn() {
    return cy.xpath("//span[normalize-space()='Apply Now']");
  }

  get_courseAndFess_silo() {
    return cy.xpath("//a[normalize-space()='Courses & Fees']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') + '/canada/college/104-university-of-toronto-toronto'
    );
  }

  verify_SSR_and_client_nav() {
    this.get_courseAndFess_silo().should('contain', 'Courses');
    this.get_courseAndFess_silo().last().click({ force: true });
    cy.url().should('include', 'programs');
    cy.go('back');
    this.get_courseAndFess_silo().should('contain', 'Courses');
    cy.reload();
    this.get_courseAndFess_silo().should('contain', 'Courses');
  }
}
export default SA_College_Home_PO;
