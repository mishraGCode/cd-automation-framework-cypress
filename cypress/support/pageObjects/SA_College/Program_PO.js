class Program_PO {
  get_heading() {
    return cy.xpath("//*[normalize-space()='Program Details']");
  }

  get_career_link() {
    return cy.xpath("//*[normalize-space()='Careers']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') +
        '/canada/college/104-university-of-toronto-toronto/master-of-business-administration-mba-18172'
    );
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_heading(), null);
    this.get_career_link().last().click({ force: true });
    cy.url().should('include', 'career');
    cy.go('back');
    cy.ValidateElement(this.get_heading(), null);
    cy.reload();
    cy.ValidateElement(this.get_heading(), null);
  }
}
export default Program_PO;
