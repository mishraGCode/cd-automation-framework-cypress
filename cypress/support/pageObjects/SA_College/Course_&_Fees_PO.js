class Course_And_fees_PO {
  get_business_stream() {
    return cy.xpath("//a[normalize-space()='Business']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') +
        '/canada/college/104-university-of-toronto-toronto/programs'
    );
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_business_stream(), null);
    this.get_business_stream().click({ force: true });
    cy.url().should('include', 'stream');
    cy.go('back');
    cy.ValidateElement(this.get_business_stream(), null);
    cy.reload();
    cy.ValidateElement(this.get_business_stream(), null);
  }
}
export default Course_And_fees_PO;
