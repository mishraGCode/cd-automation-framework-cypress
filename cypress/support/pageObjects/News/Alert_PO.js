class Alert_PO {
  get_news_breadcrumb() {
    return cy.xpath("//span[normalize-space()='News']");
  }

  getCareerLink() {
    return cy.xpath("//*[normalize-space()='Careers']");
  }

  visitPage() {
    if (
      Cypress.env('stage') === 'https://collegedunia.com' ||
      Cypress.env('stage') === 'https://beta2.collegedunia.com'
    ) {
      cy.visit(
        Cypress.env('stage') +
          '/news/bit-deoghar-btech-lateral-entry-admission-2022-open-alertid-58283'
      );
    } else {
      cy.visit(
        Cypress.env('stage') +
          '/news/aiims-jammu-csir-iiim-jammu-ink-mou-to-conduct-scientific-research-alertid-48077'
      );
    }
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_news_breadcrumb(), null);
    this.getCareerLink().last().click({ force: true });
    cy.url().should('include', 'career');
    cy.go('back');
    cy.ValidateElement(this.get_news_breadcrumb(), null);
    cy.reload();
    cy.ValidateElement(this.get_news_breadcrumb(), null);
  }
}
export default Alert_PO;
