class G_News_PO {
  get_news_breadcrumb() {
    return cy.xpath("//span[normalize-space()='News']");
  }

  getCareerLink() {
    return cy.xpath("//*[normalize-space()='Careers']");
  }

  visitPage() {
    if (
      Cypress.env('stage') === 'https://collegedunia.com' ||
      Cypress.env('stage') === 'https://beta2.collegedunia.com'
    ) {
      cy.visit(
        Cypress.env('stage') +
          '/news/g-14861-why-extra-curriculars-and-inter-personal-skills-are-as-important-as-academics'
      );
    } else {
      cy.visit(Cypress.env('stage') + '/news/g-22828-btech-admission');
    }
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_news_breadcrumb(), null);
    this.getCareerLink().last().click({ force: true });
    cy.url().should('include', 'career');
    cy.go('back');
    cy.ValidateElement(this.get_news_breadcrumb(), null);
    cy.reload();
    cy.ValidateElement(this.get_news_breadcrumb(), null);
  }
}
export default G_News_PO;
