class E_News_PO {
  get_college_predictor_silo() {
    return cy.xpath("//*[normalize-space()='College Predictor']");
  }

  get_career_link() {
    return cy.xpath("//*[normalize-space()='Careers']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') +
        '/news/e-457-neet-2021-question-paper-with-solution-pdf-in-kannada-p5'
    );
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_college_predictor_silo(), null);
    this.get_career_link().last().click({ force: true });
    cy.url().should('include', 'career');
    cy.go('back');
    cy.ValidateElement(this.get_career_link(), null);
    cy.reload();
    cy.ValidateElement(this.get_career_link(), null);
  }
}
export default E_News_PO;
