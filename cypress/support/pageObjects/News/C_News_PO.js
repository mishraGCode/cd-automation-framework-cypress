class C_News_PO {
  get_info_silo() {
    return cy.xpath("//a[normalize-space()='Courses & Fees']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') + '/news/c-25825-mds-university-ajmer-results-2017'
    );
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_info_silo(), null);
    this.get_info_silo().last().click({ force: true });
    cy.url().should('include', 'course');
    cy.go('back');
    cy.ValidateElement(this.get_info_silo(), null);
    cy.reload();
    cy.ValidateElement(this.get_info_silo(), null);
  }
}
export default C_News_PO;
