class Home_PO {
  get_next_page_button() {
    return cy.xpath("//*[normalize-space()='Next']");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/reviews');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_next_page_button(), null);
    this.get_next_page_button().last().click({ force: true });
    cy.url().should('include', '2');
    cy.go('back');
    cy.ValidateElement(this.get_next_page_button(), null);
    cy.reload();
    cy.ValidateElement(this.get_next_page_button(), null);
  }
}
export default Home_PO;
