class Write_Review_2_PO {
  get_random_heading_text() {
    return cy.xpath("//*[normalize-space()='1.1 Personal Info *']");
  }

  get_career_link() {
    return cy.xpath("//*[normalize-space()='Careers']");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/write-review-2');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_random_heading_text(), null);
    this.get_career_link().last().click({ force: true });
    cy.url().should('include', 'fail');
    cy.go('back');
    cy.ValidateElement(this.get_random_heading_text(), null);
    cy.reload();
    cy.ValidateElement(this.get_random_heading_text(), null);
  }
}
export default Write_Review_2_PO;
