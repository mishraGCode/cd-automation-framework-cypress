class CD_Review_PO {
  get_reviews_breadcrumb() {
    return cy.xpath("//span[normalize-space()='Reviews']");
  }
  visitPage() {
    cy.visit(
      Cypress.env('stage') +
        '/reviews/179089-tejas-morande-review-on-fergusson-college-pune'
    );
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_reviews_breadcrumb(), null);
    this.get_reviews_breadcrumb().click({ force: true });
    cy.url().should('include', 'reviews');
    cy.go('back');
    cy.ValidateElement(this.get_reviews_breadcrumb(), null);
    cy.reload();
    cy.ValidateElement(this.get_reviews_breadcrumb(), null);
  }
}
export default CD_Review_PO;
