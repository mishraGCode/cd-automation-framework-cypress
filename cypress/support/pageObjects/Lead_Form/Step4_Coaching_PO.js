class Step4_Coaching_PO {
  enter_college(clgName) {
    cy.get('#college_name-0-label').click({ force: true });
    cy.get("Input[placeholder='Select College']").last().type('lolove');
    cy.xpath(
      "//p[contains(text(),'Lovely Professional University - [LPU]')]"
    ).click({ force: true });
  }
  enterStatus() {
    cy.get('#status-0-label').click();
    cy.xpath("//li[normalize-space()='Interested']").click();
  }
  enterYr() {
    cy.xpath("//input[@name='study_start']").click();
    cy.xpath("//li[normalize-space()='2022']").click();
  }
  clickSubmitBtn() {
    cy.xpath("//button[normalize-space()='Submit']").click();
  }

  fourthStepSubmissionMinIp() {
    this.enter_college();
    this.enterStatus();
    this.enterYr();
    this.clickSubmitBtn();
  }
}
export default Step4_Coaching_PO;
