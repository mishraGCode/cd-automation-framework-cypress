class Step_1_PO {
  getName() {
    return cy.get('#leadname');
  }
  getEmail() {
    return cy.get('#leademail');
  }
  getMobile() {
    return cy.get('#leadphone_no');
  }
  getCity() {
    return cy.get('#leadcity');
  }

  getCourse() {
    return cy.xpath("//input[@id='leadcourse_tag_id']");
  }

  clickSubmitBtn() {
    return cy.get("button[class*='btn btn-primary btn']").last().click();
  }
  enterCity() {
    cy.xpath("//li[normalize-space()='New Delhi']").click();
  }
  enterCourseTag() {
    cy.get('#leadcourse_tag_id').click();
    cy.get('.custom-search-head')
      .contains('Popular')
      .siblings('.custom-search-submenu')
      .find('.custom-search-submenu-option')
      .eq(0)
      .click();
  }

  firstStepSubmission(name, email, mobile, city) {
    this.getName().type(name);
    this.getEmail().type(email);
    this.getMobile().type(mobile);
    this.getCity().type(city);
    this.enterCity();
    this.enterCourseTag();
    this.clickSubmitBtn();
  }
}
export default Step_1_PO;
