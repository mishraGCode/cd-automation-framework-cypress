class Step2_Coaching_PO {
  sel_coach_radio_btn() {
    cy.get("label[for='coaching-0']").click();
  }

  clickNextBtn() {
    cy.xpath("//button[normalize-space()='Next']").click();
  }

  second_step_submission_min_ip() {
    this.sel_coach_radio_btn();
    this.clickNextBtn();
  }
}
export default Step2_Coaching_PO;
