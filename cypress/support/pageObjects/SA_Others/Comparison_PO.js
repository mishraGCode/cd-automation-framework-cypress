class Comparison_PO {
  get_heading() {
    return cy.xpath("//h1[contains(text(),'Arizona')]");
  }

  get_author() {
    return cy.xpath("//a[normalize-space()='Sayantani Barman']");
  }

  visitPage() {
    if (
      Cypress.env('stage') === 'https://collegedunia.com' ||
      Cypress.env('stage') === 'https://beta2.collegedunia.com'
    ) {
      cy.visit(
        Cypress.env('stage') +
          '/comparison/2110-arizona-state-university-vs-942-georgia-institute-of-technology'
      );
    } else {
      cy.visit(
        Cypress.env('stage') +
          '/comparison/2110-arizona-state-university-vs-1634-georgia-college-and-state-university'
      );
    }
  }

  verify_SSR_and_client_nav() {
    this.get_heading().should('contain', 'Arizona');
    this.get_author().invoke('removeAttr', 'target').click({ force: true });
    cy.url().should('include', 'sayantani');
    cy.go('back');
    this.get_heading().should('contain', 'Arizona');
    cy.reload();
    this.get_heading().should('contain', 'Arizona');
  }
}
export default Comparison_PO;
