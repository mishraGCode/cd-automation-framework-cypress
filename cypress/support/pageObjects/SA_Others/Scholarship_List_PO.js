class Scholarship_List_PO {
  get_scholarship_link() {
    return cy.xpath(
      "//*[normalize-space()='Hubert H. Humphrey Fellowship Program']"
    );
  }

  getCareerLink() {
    return cy.xpath("//*[normalize-space()='Careers']");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/scholarship');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_scholarship_link(), null);
    this.getCareerLink().last().click({ force: true });
    cy.url().should('include', 'career');
    cy.go('back');
    cy.ValidateElement(this.get_scholarship_link(), null);
    cy.reload();
    cy.ValidateElement(this.get_scholarship_link(), null);
  }
}
export default Scholarship_List_PO;
