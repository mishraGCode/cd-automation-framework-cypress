class Article_Canada_PO {
  get_clg_link() {
    return cy.xpath("//*[normalize-space()='University of Toronto']");
  }

  getCareerLink() {
    return cy.xpath("//*[normalize-space()='Careers']");
  }

  visitPage() {
    cy.visit(
      Cypress.env('stage') +
        '/canada/article/study-in-canada-cost-scholarship-visa-requirements'
    );
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_clg_link(), null);
    this.getCareerLink().last().click({ force: true });
    cy.url().should('include', '');
    cy.go('back');
    cy.ValidateElement(this.get_clg_link(), null);
    cy.reload();
    cy.ValidateElement(this.get_clg_link(), null);
  }
}
export default Article_Canada_PO;
