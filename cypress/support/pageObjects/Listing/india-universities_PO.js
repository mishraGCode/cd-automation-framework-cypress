class India_Universities_PO {
  get_select_stream_heading() {
    return cy.xpath("//*[normalize-space()='Select Stream']");
  }

  get_courseAndFess_link() {
    return cy.xpath(
      "//*[normalize-space()='IIM Bangalore - Indian Institute of Management']"
    );
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/india-universities');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_select_stream_heading(), null);
    this.get_courseAndFess_link().last().click({ force: true });
    cy.url().should('include', 'bangalore');
    cy.go('back');
    cy.ValidateElement(this.get_select_stream_heading(), null);
    cy.reload();
    cy.ValidateElement(this.get_select_stream_heading(), null);
  }
}
export default India_Universities_PO;
