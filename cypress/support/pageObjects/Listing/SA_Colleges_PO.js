class SA_Colleges_PO {
  get_select_stream_heading() {
    return cy.xpath("//*[normalize-space()='Select Stream']");
  }

  get_college_link() {
    return cy.xpath("//*[normalize-space()='University of Oxford']");
  }

  visitPage() {
    cy.visit(Cypress.env('stage') + '/study-abroad-colleges');
  }

  verify_SSR_and_client_nav() {
    cy.ValidateElement(this.get_college_link(), null);
    this.get_college_link().first().click({ force: true });
    cy.url().should('include', 'oxford');
    cy.go('back');
    cy.ValidateElement(this.get_college_link(), null);
    cy.reload();
    cy.ValidateElement(this.get_college_link(), null);
  }
}
export default SA_Colleges_PO;
