// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })

// Cypress.Commands.add('validate_SSR_and_client_navigation', () => {
//   cy.get('[name="first_name"]').type(firstName);
//   cy.get('[name="last_name"]').type(lastName);
//   cy.get('[name="email"]').type(email);
//   cy.get('textarea.feedback-input').type(comment);
//   cy.get('[type="submit"]').click();
//   cy.get($selector).contains(textToLocate);
// });
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
// Add this to cypress/support/commands.js

Cypress.Commands.add('ValidateElement', (xpath, text) => {
  cy.get(xpath).then(($element) => {
    if ($element.is(':visible')) {
      //you get here only if button is visible
      expect(xpath).to.exist;
    }
  });
  if (text != null) {
    cy.get(xpath).should((xpath) => {
      expect(xpath).to.contain(text);
    });
  }
});

import 'cypress-file-upload';
